defmodule VisableBankWeb.AccountControllerTest do
  use VisableBankWeb.ConnCase

  alias VisableBank.Accounts.Projections.Account

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "show/2 renders account when data is valid", %{conn: conn} do
    %Account{uuid: uuid} = insert(:account, current_balance: 100_000)

    conn = get(conn, Routes.account_path(conn, :show, uuid))

    assert %{
             "uuid" => uuid,
             "current_balance" => "1,000.00 €"
           } = json_response(conn, 200)["data"]
  end
end
