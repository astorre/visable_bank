{:ok, _} = Application.ensure_all_started(:faker)
{:ok, _} = Application.ensure_all_started(:ex_machina)
Ecto.Adapters.SQL.Sandbox.mode(VisableBank.Repo, :manual)

ExUnit.start()
