defmodule VisableBank.AccountsTest do
  use VisableBank.DataCase

  alias VisableBank.Accounts

  describe "accounts" do
    alias VisableBank.Accounts.Projections.Account

    @valid_attrs %{"initial_balance" => 42}
    @invalid_attrs %{"initial_balance" => nil}

    def account_fixture(attrs \\ %{}) do
      {:ok, account} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_account()

      account
    end

    test "get_account/1 returns the account with given id" do
      %Account{uuid: uuid, current_balance: current_balance} = account = insert(:account)

      assert {:ok, %Account{uuid: ^uuid, current_balance: %Money{amount: ^current_balance, currency: :EUR}}} =
               Accounts.get_account(uuid)
    end

    test "open_account/1 with valid data creates a account" do
      assert {:ok, %Account{current_balance: current_balance}} = Accounts.open_account(@valid_attrs)
      assert %Money{amount: 42, currency: :EUR} = current_balance
    end
  end
end
