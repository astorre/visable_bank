defmodule VisableBank.Factory do
  use ExMachina.Ecto, repo: VisableBank.Repo

  alias VisableBank.Accounts.Projections.Account

  def account_factory do
    %Account{
      uuid: UUID.uuid4(),
      current_balance: 0..1_000_000 |> Enum.random() |> Money.new()
    }
  end
end
