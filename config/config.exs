# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :visable_bank,
  ecto_repos: [VisableBank.Repo]

# Configures the endpoint
config :visable_bank, VisableBankWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "lcGrovfMngMo/JrY1/4WVrlZDYLbPsEbvaQHsS7XnzRpktycXpNtYuYd3xAeEmfA",
  render_errors: [view: VisableBankWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: VisableBank.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :money, default_currency: :EUR

config :commanded,
  event_store_adapter: Commanded.EventStore.Adapters.EventStore

config :commanded_ecto_projections,
  repo: VisableBank.Repo

config :visable_bank, transactions_history_size: 10

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
