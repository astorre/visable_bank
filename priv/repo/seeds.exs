# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     VisableBank.Repo.insert!(%VisableBank.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias VisableBank.Accounts

account_id1 = "4982820a-fbec-4f2e-bfeb-9c320934bc7a"
account_id2 = "00ade8d3-8513-46ba-98a2-8583ca4ac332"

init_params1 = %{"uuid" => account_id1, "initial_balance" => 10_000}
init_params2 = %{"uuid" => account_id2, "initial_balance" => 10_000}

with {:error, :not_found} <- Accounts.get_account(account_id1) do
  Accounts.open_account(init_params1)
end

with {:error, :not_found} <- Accounts.get_account(account_id2) do
  Accounts.open_account(init_params2)
end
