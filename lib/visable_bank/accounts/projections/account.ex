defmodule VisableBank.Accounts.Projections.Account do
  use Ecto.Schema

  @primary_key {:uuid, :binary_id, autogenerate: false}

  schema "accounts" do
    field :current_balance, Money.Ecto.Amount.Type

    timestamps()
  end
end
