defmodule VisableBank.Router do
  @moduledoc """
  CQRS router.
  """
  use Commanded.Commands.Router

  alias VisableBank.Accounts.Aggregates.Account

  alias VisableBank.Accounts.Commands.{
    OpenAccount,
    DepositIntoAccount,
    WithdrawFromAccount,
    TransferBetweenAccounts
  }

  dispatch(
    [
      OpenAccount,
      DepositIntoAccount,
      WithdrawFromAccount,
      TransferBetweenAccounts
    ],
    to: Account,
    identity: :account_uuid
  )
end
