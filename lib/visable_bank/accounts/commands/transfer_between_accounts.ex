defmodule VisableBank.Accounts.Commands.TransferBetweenAccounts do
  @enforce_keys [:account_uuid, :transfer_uuid]

  defstruct [:account_uuid, :transfer_uuid, :transfer_amount, :destination_account_uuid]

  alias VisableBank.Repo
  alias VisableBank.Accounts.Projections.Account

  def valid?(command) do
    cmd = Map.from_struct(command)

    with %Account{} <- account_exists?(cmd.destination_account_uuid) do
      true
    else
      nil ->
        {:error, ["Destination account does not exist"]}

      reply ->
        reply
    end
  end

  defp account_exists?(uuid) do
    Repo.get(Account, uuid)
  end
end
