defmodule VisableBank.Accounts.Commands.DepositIntoAccount do
  @enforce_keys [:account_uuid]

  defstruct [:account_uuid, :deposit_amount, :transfer_uuid]
end
