defmodule VisableBank.Accounts.Commands.WithdrawFromAccount do
  @enforce_keys [:account_uuid]

  defstruct [:account_uuid, :withdraw_amount, :transfer_uuid]
end
