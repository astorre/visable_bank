defmodule VisableBank.Accounts.Supervisor do
  @moduledoc """
  CQRS supervisor.
  """
  use Supervisor

  alias VisableBank.Accounts.Projectors
  alias VisableBank.Accounts.ProcessManagers

  def start_link(_) do
    Supervisor.start_link(__MODULE__, nil)
  end

  def init(_arg) do
    children = [
      worker(Projectors.AccountOpened, [], id: :account_opened),
      worker(Projectors.DepositsAndWithdrawals, [], id: :deposits_and_withdrawals),
      worker(ProcessManagers.TransferMoney, [], id: :transfer_money)
    ]

    supervise(children, strategy: :one_for_one)
  end
end
