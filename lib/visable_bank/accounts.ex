defmodule VisableBank.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias VisableBank.{Repo, Router}

  alias VisableBank.Accounts.Commands.{
    OpenAccount,
    TransferBetweenAccounts
  }

  alias VisableBank.Accounts.Projections.Account

  @doc """
  Gets a single account.

  ## Examples

      iex> get_account(05188d0e-d6b5-4e84-87dd-a7c9f6d0906a)
      {:ok, %Account{}}

      iex> get_account(d15a63ae-5200-423b-887a-390d93f0202a)
      {:error, :not_found}

  """
  def get_account(id) do
    case Repo.get(Account, id) do
      %Account{} = account ->
        {:ok, account}

      _reply ->
        {:error, :not_found}
    end
  end

  def open_account(%{"initial_balance" => initial_balance} = params) do
    account_uuid =
      case params do
        %{"uuid" => uuid} -> uuid
        _ -> UUID.uuid4()
      end

    dispatch_result =
      %OpenAccount{
        initial_balance: initial_balance,
        account_uuid: account_uuid
      }
      |> Router.dispatch(consistency: :strong)

    case dispatch_result do
      :ok -> get_account(account_uuid)
      reply -> reply
    end
  end

  def open_account(_params), do: {:error, :bad_command}

  def transfer(source_id, amount, destination_id) do
    %TransferBetweenAccounts{
      account_uuid: source_id,
      transfer_uuid: UUID.uuid4(),
      transfer_amount: amount,
      destination_account_uuid: destination_id
    }
    |> Router.dispatch()
  end
end
