defmodule VisableBank.Repo do
  use Ecto.Repo,
    otp_app: :visable_bank,
    adapter: Ecto.Adapters.Postgres
end
