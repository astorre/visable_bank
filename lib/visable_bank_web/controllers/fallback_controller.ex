defmodule VisableBankWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use VisableBankWeb, :controller

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(VisableBankWeb.ErrorView)
    |> render(:"404")
  end

  def call(conn, {:validation_error, _changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(VisableBankWeb.ErrorView)
    |> render(:"422")
  end

  def call(conn, {:error, :insufficient_funds}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(VisableBankWeb.ErrorView)
    |> assign(:message, "Insufficient funds to process order")
    |> render(:"422")
  end

  def call(conn, {:error, :transfer_to_same_account}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(VisableBankWeb.ErrorView)
    |> assign(:message, "Source and destination accounts are the same")
    |> render(:"422")
  end
end
