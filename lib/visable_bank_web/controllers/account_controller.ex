defmodule VisableBankWeb.AccountController do
  use VisableBankWeb, :controller

  alias VisableBank.Accounts
  alias VisableBank.Accounts.Projections.Account

  action_fallback VisableBankWeb.FallbackController

  def show(conn, %{"id" => account_id}) do
    with {:ok, %Account{} = account} <- Accounts.get_account(account_id) do
      render(conn, "show.json", account: account)
    end
  end

  def transfer(
        conn,
        %{
          "id" => account_id,
          "transfer_amount" => amount,
          "destination_account" => destination_account_id
        }
      ) do
    with :ok <-
           Accounts.transfer(account_id, amount, destination_account_id) do
      send_resp(conn, 201, "")
    end
  end
end
