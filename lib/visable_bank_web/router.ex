defmodule VisableBankWeb.Router do
  use VisableBankWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", VisableBankWeb do
    pipe_through :api

    resources "/accounts", AccountController, only: [:show]
    post "/accounts/:id/transfer", AccountController, :transfer
  end
end
