defmodule VisableBankWeb.TransactionView do
  use VisableBankWeb, :view

  alias VisableBankWeb.MoneyHelpers

  def render("transaction.json", %{transaction: %{data: %{amount: amount} = data}}) do
    %{data | amount: MoneyHelpers.balance_to_money(amount)}
  end
end
