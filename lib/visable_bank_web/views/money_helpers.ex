defmodule VisableBankWeb.MoneyHelpers do
  @moduledoc """
  Helper functions for formating money data.
  """

  def balance_to_money(current_balance) when is_integer(current_balance) do
    current_balance
    |> Money.new()
    |> Money.to_string(symbol: true, symbol_on_right: true, symbol_space: true)
  end

  def balance_to_money(current_balance) do
    Money.to_string(current_balance, symbol: true, symbol_on_right: true, symbol_space: true)
  end
end
