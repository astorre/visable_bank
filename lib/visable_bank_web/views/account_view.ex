defmodule VisableBankWeb.AccountView do
  use VisableBankWeb, :view

  alias VisableBankWeb.{AccountView, MoneyHelpers, TransactionView}
  alias VisableBank.Accounts.Projections.Account

  @history_size Application.fetch_env!(:visable_bank, :transactions_history_size)

  def render("show.json", %{account: account}) do
    %{data: render_one(account, AccountView, "account.json")}
  end

  def render("account.json", %{account: %Account{uuid: uuid, current_balance: current_balance}}) do
    %{
      uuid: uuid,
      current_balance: MoneyHelpers.balance_to_money(current_balance),
      transactions: render_transactions(uuid)
    }
  end

  def render("account.json", _params), do: %{}

  defp render_transactions(uuid) do
    uuid
    |> (&if(Mix.env() == :test, do: [], else: transactions_by(&1))).()
    |> render_many(TransactionView, "transaction.json")
  end

  defp transactions_by(uuid) do
    EventStore.stream_all_forward()
    |> Stream.filter(fn
      %EventStore.RecordedEvent{
        data: %VisableBank.Accounts.Events.MoneyTransferRequested{
          destination_account_uuid: destination_account_uuid,
          source_account_uuid: source_account_uuid
        }
      } ->
        uuid in [destination_account_uuid, source_account_uuid]

      _ ->
        false
    end)
    |> Enum.sort(&(&1.event_number >= &2.event_number))
    |> Enum.take(@history_size)
  end
end
