# VisableBank

_Elixir Developer Coding Exercise: Dmitry Vysotsky_

## Setup

Fetch dependencies:

```sh
mix deps.get
```

Create DB and run migrations:

```sh
mix do event_store.create, event_store.init
mix do ecto.create, ecto.migrate
```

Run seeds:

```sh
mix run priv/repo/seeds.exs
```

And run project:

```sh
iex -S mix phx.server
```

In case of total system reset, faster way is run these commands:

```sh
mix do event_store.drop, event_store.create, event_store.init
mix do ecto.drop, ecto.create, ecto.migrate
mix run priv/repo/seeds.exs
iex -S mix phx.server
```
